package com.bradkarels.simple

import java.util.Properties

import scala.annotation.tailrec
import scala.util.Random

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.joda.time.DateTime

import kafka.producer.KeyedMessage
import kafka.producer.Producer
import kafka.producer.ProducerConfig

object RandomMessages {
  
  def main(args: Array[String]) {
    val topic:String = if (args.length > 0) args(0) else "sparkfu"
    val secondsToRun = if (args.length > 1) args(1).toInt else 10
    val printProgress:Boolean = if (args.length > 2) {
      args(2).equalsIgnoreCase("true")
    } else false
    
    blather(printProgress, s"This will run for: ${secondsToRun}s")
    
	val conf = new SparkConf().setAppName("Spark Fu Kafka Spewer")
	val sc = new SparkContext(conf)
	
	val props:Properties = new Properties()
        props.put("metadata.broker.list", "localhost:9092")
        props.put("serializer.class", "kafka.serializer.StringEncoder")
 
    val config = new ProducerConfig(props)
    val producer = new Producer[String, String](config)
    
    val start:DateTime = DateTime.now()
    val startMillis:Long = start.getMillis()
    val then:DateTime = start.plusSeconds(secondsToRun)
    
    var cnt:Int = 0
    var remaining:Int = secondsToRun
    
    while (then.isAfterNow()) {
      val nowMillis = DateTime.now().getMillis()
      if (printProgress && cnt % 5000 == 0) {
        val nowRemaining = (secondsToRun - (nowMillis - startMillis)/1000).toInt
        if (nowRemaining < remaining) {
          remaining = nowRemaining
          if (remaining > 0) println(s"${cnt} * ${remaining} seconds left...")
          else println("${cnt} * Less than one second remaining...")
        }
      }
      // Some fake "processing"...
      val rnd500:String = randomString(500)
      val rnd50:String = rnd500.take(50)
      val rnd25:String = rnd50.takeRight(25)
	  val msg = s"${nowMillis},${rnd25},${rnd50},${rnd500}" // Add millis as an "id".
      producer.send(new KeyedMessage[String, String]("sparkfu", msg))
      cnt += 1
    }
    
    val mps:Float = cnt/secondsToRun
    blather(printProgress, s"Produced ${cnt} msgs in ${secondsToRun}s -> ${mps}m/s.")
  }
  
  def blather(p:Boolean, msg:String):Unit = {
    if (p) println(msg)
  }

  @tailrec
  def doRandomString(n: Int, charSet:Seq[Char], list: List[Char]): List[Char] = {
	val rndPosition = Random.nextInt(charSet.length)
	val rndChar = charSet(rndPosition)
    if (n == 1) rndChar :: list
    else doRandomString(n - 1, charSet, rndChar :: list)
  }

  def randomString(n: Int): String = {
    val chars = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9')
    doRandomString(n, chars, Nil).mkString
  }
}